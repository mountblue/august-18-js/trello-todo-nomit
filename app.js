const key = '8586558257d25879ee0ca230e9ecbdd0';
const token = '5d7db47d574ddf9563e44996db51601a95afe7f081a4aa2dee7c79a8c3a5c67f';
const boardId = '5b7a62fa6dc3387808e7bd6d';


$(document).ready(() => {
  fetch(
    `https://api.trello.com/1/boards/${boardId}/cards?fields=all,url&key=${key}&token=${token}`,
  )
    .then(response => response.json())
    .then(cardList => getCards(cardList))
    .catch((err) => {
      console.log(err.message);
    });
});
function getCards(cardList) {
  cardList.forEach((card) => {
    fetch(
      `https://api.trello.com/1/cards/${
        card.id
      }/checklists?fields=all,url&key=${key}&token=${token}`,
    )
      .then(response => response.json())
      .then(checklists => getChecklists(checklists, card.id))
      .then(() => updateCheckitem())
      .catch((err) => {
        console.log(err.message);
      });
  });
}
function getChecklists(checklistArray, cardId) {
  checklistArray.forEach((checklist) => {
    checklist.checkItems.forEach((checkitem) => {
      if (checkitem.state === 'incomplete') {
        $('.list-group').append(
          `<li class='list-group-item'><input type='checkbox' data-cardId='${cardId}'value='${
            checkitem.id
          }'><label>${checkitem.name}</label></li>`,
        );
      }
    });
  });
}
function updateCheckitem() {
  $('input').click(function () {
    const checkitemId = $(this).val();
    const cardId = $(this).attr('data-cardId');
    let status;
    if ($(this).is(':checked')) status = 'complete';
    else status = 'incomplete';
    fetch(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}/?state=${status}&url&key=${key}&token=${token}`,
      {
        method: 'PUT',
      },
    ).catch((err) => {
      console.log(err.message);
    });
  });
}
